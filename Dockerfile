FROM openjdk:11-jdk
COPY target/*.jar employee-service.jar
ENTRYPOINT ["java", "-jar", "/employee-service.jar"]
EXPOSE 8081