package com.aycap.service.employee

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.*

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
data class Employee (
    @Id
    var employeeId: String,
    var name: String,
    var age: Int = 0,
    var position: String,
    var organizationId: String,
    var departmentId: String
)

