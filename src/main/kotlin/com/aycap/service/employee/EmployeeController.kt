package com.aycap.service.employee

import com.aycap.service.payload.CreateEmployeeRequest
import com.aycap.service.payload.FindEmployeeResponse
import com.aycap.service.payload.ResponseMessage
import com.aycap.service.util.SystemTimeUtil
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/employees")
class EmployeeController(
    private val employeeService: EmployeeService,
    private val systemTimeUtil: SystemTimeUtil
) {

    @GetMapping
    fun findAllEmployee(): ResponseEntity<ResponseMessage> {
        val startDateTime = systemTimeUtil.getCurrentDateTime()
        println(String.format("[ GET /employees ] Start: %s", startDateTime))

        val employeeList = employeeService.findAllEmployee()
        val response = ResponseMessage(
            code = "200",
            messageTh = "ทำรายการสำเร็จ",
            messageEn = "Success",
            responseData = employeeList
        )

        val endDateTime = systemTimeUtil.getCurrentDateTime()
        println(String.format("[ GET /employees ] End: %s", endDateTime))

        return ResponseEntity.ok(response)
    }

    @GetMapping("/{employeeId}")
    fun findEmployee(@PathVariable employeeId: String): ResponseEntity<Any> {
        val employee = employeeService.findEmployeeById(employeeId)
        return ResponseEntity.ok(employee)
    }

    @GetMapping("/department/{departmentId}")
    fun findEmployeeByDepartmentId(@PathVariable departmentId: String): List<FindEmployeeResponse> {
        return employeeService.findEmployeeByDepartmentId(departmentId)
    }

    @GetMapping("/organization/{organizationId}")
    fun findEmployeeByOrganizationId(@PathVariable organizationId: String): List<FindEmployeeResponse> {
        return employeeService.findEmployeeByOrganizationId(organizationId)
    }

    @PostMapping(
        produces = [MediaType.APPLICATION_JSON_VALUE],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun addEmployee(@RequestBody req: CreateEmployeeRequest): ResponseEntity<Any> {
        val employee = employeeService.addNewEmployee(req)
        return ResponseEntity.ok(employee)
    }

}