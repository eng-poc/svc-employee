package com.aycap.service.employee

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository: JpaRepository<Employee, String> {
    fun findByDepartmentId(departmentId: String): List<Employee>
    fun findByOrganizationId(organizationId: String): List<Employee>
}