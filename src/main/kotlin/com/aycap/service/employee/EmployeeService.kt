package com.aycap.service.employee

import com.aycap.service.payload.CreateEmployeeRequest
import com.aycap.service.payload.CreateEmployeeResponse
import com.aycap.service.payload.FindEmployeeResponse
import org.springframework.stereotype.Service
import java.util.*

@Service
class EmployeeService(
    private val employeeRepository: EmployeeRepository
) {

    fun findAllEmployee(): List<FindEmployeeResponse> {
        val employeeList = employeeRepository.findAll()
        return employeeListToDTO(employeeList)
    }

    fun findEmployeeByDepartmentId(departmentId: String): List<FindEmployeeResponse> {
        val employeeList = employeeRepository.findByDepartmentId(departmentId)
        return employeeListToDTO(employeeList)
    }

    fun findEmployeeByOrganizationId(organizationId: String): List<FindEmployeeResponse> {
        val employeeList = employeeRepository.findByOrganizationId(organizationId)
        return employeeListToDTO(employeeList)
    }

    fun findEmployeeById(id: String): FindEmployeeResponse {
        val employee = employeeRepository.findById(id).orElse(null)
        return FindEmployeeResponse(employee.employeeId, employee.name, employee.age, employee.position, employee.organizationId, employee.departmentId)
    }

    fun addNewEmployee(req: CreateEmployeeRequest): CreateEmployeeResponse {
        val newEmployee = employeeRepository.save(
            Employee(
                employeeId = UUID.randomUUID().toString(),
                name = req.name,
                age = req.age.toInt(),
                position = req.position,
                organizationId = req.organizationId,
                departmentId = req.departmentId
            )
        )
        return CreateEmployeeResponse(newEmployee.employeeId, newEmployee.name)
    }

    internal fun employeeListToDTO(employeeList: List<Employee>): List<FindEmployeeResponse> {
        return employeeList.map { FindEmployeeResponse(it.employeeId, it.name, it.age, it.position, it.organizationId, it.departmentId) }
    }

}