package com.aycap.service.payload

class CreateEmployeeRequest(
    var name: String,
    var age: String,
    var position: String,
    var organizationId: String,
    var departmentId: String
)