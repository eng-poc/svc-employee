package com.aycap.service.payload

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
class CreateEmployeeResponse(
    var employeeId: String,
    var name: String
)