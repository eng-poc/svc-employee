package com.aycap.service.payload

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
class FindEmployeeResponse(
    var employeeId: String,
    var name: String,
    var age: Int,
    var position: String,
    var organizationId: String,
    var departmentId: String
)