package com.aycap.service.payload

class ResponseMessage(
    var code: String,
    var messageTh: String,
    var messageEn: String,
    var responseData: Any? = null
)