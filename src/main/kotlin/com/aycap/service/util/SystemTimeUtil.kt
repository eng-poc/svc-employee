package com.aycap.service.util

import org.springframework.stereotype.Service
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.math.abs

@Service
class SystemTimeUtil {
     val calendar: Calendar = Calendar.getInstance()
    val DATE_HYPHEN_FORMAT_FULL: String = "dd-MM-yyyy HH:mm:ss.SSS"

    fun getCurrentDateTime(): String {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_HYPHEN_FORMAT_FULL)).toString()
        // return LocalDateTime.now().toString()
    }

    fun getCurrentMillis(): Long {
        println(System.currentTimeMillis())
        return System.currentTimeMillis()
    }

    fun convertLongToDatetime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat(DATE_HYPHEN_FORMAT_FULL)
        return format.format(date)
    }

    fun calProcessTime(start: Long, end: Long): Long {
        return abs(end - start)
    }
}